package de.dieunkreativen.flatlands;

import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.block.Biome;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.world.ChunkLoadEvent;
import org.bukkit.generator.ChunkGenerator;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.java.JavaPlugin;

public class Flatlands extends JavaPlugin implements Listener {
	Logger log;
	PluginDescriptionFile pdfFile = this.getDescription();

    public void onLoad() {
    	log = getLogger();
    }
    
	@Override
	public void onEnable() {
        log.info(pdfFile.getName() + " version " + pdfFile.getVersion() + " is enabled!");
	}
	
	@Override
	public void onDisable() {
		log.info(pdfFile.getName() + " version " + pdfFile.getVersion() + " is disabled!");
	}
	
	public ChunkGenerator getDefaultWorldGenerator(String worldName, String uid) {
	    return new FlatLandsGenerator(this);
	}
	
    @EventHandler
    public void onChunkLoad(ChunkLoadEvent e) {
        Chunk chunk = e.getChunk();
        setChunkBiome(chunk, Biome.FOREST);
    }
    
    public void setChunkBiome(Chunk chunk, Biome biome) {
    	for(int x = 0 ; x < 16; x++) {
    		for(int z = 0 ; z < 16; z++) {
    			if(chunk.getBlock(x, 0, z).getBiome() != biome) {
    				Block block = chunk.getBlock(x, 0, z);
    				block.setBiome(biome);
    			}
    		}
    	}
    }
}
