package de.dieunkreativen.flatlands;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Biome;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.world.ChunkLoadEvent;
import org.bukkit.generator.BlockPopulator;
import org.bukkit.generator.ChunkGenerator;

public class FlatLandsGenerator extends ChunkGenerator {
	
  public FlatLandsGenerator(Flatlands instance) {}
  
  public List<BlockPopulator> getDefauldPopulayions(World world) {
    ArrayList<BlockPopulator> populators = new ArrayList();
    return populators;
  }
  
  public Location getFixedSpawnLocation(World world, Random random) {
    return new Location(world, 0.0D, 5.0D, 0.0D);
  }
  
  private int coordsToInt(int x, int y, int z) {
    return (x * 16 + z) * 128 + y;
  }
  
  void setBlock(byte[][] result, int x, int y, int z, byte blkid) {
      if (result[y >> 4] == null) {
          result[y >> 4] = new byte[4096];
      }
      result[y >> 4][((y & 0xF) << 8) | (z << 4) | x] = blkid;
  }
 
  public byte[][] generateBlockSections(World world, Random random, int chunkX, int chunkZ, BiomeGrid biomeGrid) {
      byte[][] result = new byte[world.getMaxHeight() / 16][];
      int x, y, z;
      
      for (x = 0; x < 16; x++) {
    	  for (z = 0; z < 16; z++) {
        	  setBlock(result, x, 0, z, (byte) Material.BEDROCK.getId());
        	  for (y = 1; y < 40; y++) {
        		  setBlock(result, x, y, z, (byte) Material.DIRT.getId());
        	  }
        	  setBlock(result, x, 40, z, (byte) Material.GRASS.getId());
        	  Location loc = new Location(world, x, 40, z);
      		  biomeGrid.setBiome(x, z, Biome.FOREST);
          }
      }
          
      return result;
  }
  
  /*
  
  public byte[] generate(World world, Random rand, int chunkX, int chunkZ, BiomeGrid biomeGrid) {
    byte[] blocks = new byte[32768];
    for (int x = 0; x < 16; x++) {
      for (int z = 0; z < 16; z++) {
        blocks[coordsToInt(x, 0, z)] = ((byte)Material.BEDROCK.getId());
        for (int y = 1; y < 40; y++) {
          blocks[coordsToInt(x, y, z)] = ((byte)Material.DIRT.getId());
        }
        blocks[coordsToInt(x, 40, z)] = ((byte)Material.GRASS.getId());
        
        Location loc = new Location(world, x, 40, z);
        Block b = loc.getBlock();
		b.setBiome(Biome.ICE_PLAINS);
      }
    }
    return blocks;
  }*/
}